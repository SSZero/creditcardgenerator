# Creates a generator for credit card numbers
# utilizing the lunh algorithm
#
# Generates first 1000 numbers.

#TODO: add bin checks
def luhnCheck(cardNum):
    def digits_of(n):
        return [int(d) for d in str(n)]
    digits = digits_of(cardNum)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for d in even_digits:
        checksum += sum(digits_of(d*2))
    return checksum % 10 == 0

def generateCreditCardNumber():
    startLength = 13
    endLength = 20
    
    for num in range(0, pow(10,endLength)):
        card = str(num).rjust(startLength, '0')
        if(luhnCheck(card)):
            yield card
            

cardGenerator = generateCreditCardNumber();
for i in range(1000):
    print(next(cardGenerator))
